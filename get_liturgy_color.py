import requests
from bs4 import BeautifulSoup as bs


def remove(string):
    return "".join(string.split())


def search(values, search_for):
    for k in values:
        for v in values[k]:
            if search_for in v:
                return k
    return None


def get_liturgy_color():
    url = "https://lc.kbs.sk"

    session = requests.Session()
    session.headers[
        "User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36"

    html = session.get(url).content
    soup = bs(html, 'html.parser')

    lc_farba = soup('div')[11].contents

    liturgicka_farba = remove(lc_farba[1].attrs['title'].split(':')[1])
    print(liturgicka_farba)

    # Liturgy colors
    white = (255, 255, 255)
    red = (255, 0, 0)
    purple = (128, 0, 128)
    green = (0, 255, 0)
    gold = (255, 215, 0)
    rose = (255, 0, 127)

    s = ['BIELA', 'FIALOVÁ', 'RUŽOVÁ', 'ČERVENÁ', 'ZELENÁ']

    myDict = {'white': ['BIELA'],
              'red': ['ČERVENÁ'],
              'purple': ['FIALOVÁ'],
              'green': ['ZELENÁ'],
              'rose': ['RUŽOVÁ']}

    liturgicka_farba = search(myDict, liturgicka_farba)
    myColors = {'white': white,
              'red': red,
              'purple': purple,
              'green': green,
              'rose': rose}

    print(liturgicka_farba)
    print(myColors[liturgicka_farba])
    return myColors[liturgicka_farba]


get_liturgy_color()
