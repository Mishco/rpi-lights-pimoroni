#!/usr/bin/env python3

import time
from random import randint

import unicornhat as unicorn

# Liturgy colors
from get_liturgy_color import get_liturgy_color

white = (255, 255, 255)
red = (255, 0, 0)
purple = (128, 0, 128)
green = (0, 255, 0)
gold = (255, 215, 0)
rose = (255, 0, 127)


def remove(string):
    return "".join(string.split())


def similar_colors(rgb, loop=1):
    colors_list = []
    delta = 35
    for i in range(loop):
        new_rgb = [randint(max(0, x - delta), min(x + delta, 255)) for x in rgb]
        colors_list.append(new_rgb)
    return colors_list


unicorn.set_layout(unicorn.AUTO)
unicorn.rotation(0)
unicorn.brightness(0.5)
width, height = unicorn.get_shape()

print("Reticulations splines")
time.sleep(.5)
print("Enabled unicorn poop module!")
time.sleep(.5)
print("Pooping liturgical color...")

i = 0.0
offset = 10
liturgy_color = get_liturgy_color()
while True:
    i = i + 0.3
    for y in range(height):
        for x in range(width):
            colors = similar_colors(liturgy_color)
            unicorn.set_pixel(x, y, int(colors[0][0]), int(colors[0][1]), int(colors[0][2]))
    unicorn.show()
    time.sleep(0.08)
