#!/usr/bin/env python

import time
import unicornhat


unicornhat.set_layout(unicornhat.PHAT)
unicornhat.rotation(0)
unicornhat.brightness(0.3)

def yellow ():
    unicornhat.clear()
    unicornhat.show()
    unicornhat.set_all(255, 255, 0)
    unicornhat.show()

def green():
    unicornhat.clear()
    unicornhat.show()
    unicornhat.set_all(0, 255, 0)
    unicornhat.show()

x = 0.1
while True:
    yellow()
    time.sleep(10)
    green()
    time.sleep(10)
    x = x + 0.1
    unicornhat.brightness(x)
